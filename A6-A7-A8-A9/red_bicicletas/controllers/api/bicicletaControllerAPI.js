
let Bicicleta = require("../../models/Bicicleta");


exports.bicicleta_list = function(req,res){



/* MUESTRA TODAS LAS BICICLETAS */

Bicicleta.allBicis(function(err,bicis){
    if(err) res.status(500).send(err.message);
    res.status(200).json({
        bicicletas: bicis
    })
})
};



/* CREA UNA NUEVA BICICLETA*/

exports.bicicleta_create = function(req,res){



    let bici = new Bicicleta({ // Declaramos los parametros obtenidos por el cuerpo de Postman en una nueva clase con sus propiedades
        bicicletaID: req.body.bicicletaID,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: req.body.ubicacion
    });

    console.log(bici.bicicletaID);

    Bicicleta.add(bici,function(err,newBici){ // Llamamos al Modelo con el objeto Bici, y las respuestas CoolBack
        if(err) res.status(500).send(err.message); // Si err == TRUE
        res.status(201).send(newBici); // En caso de Código 201, imprime la Bici Creada
    });
};




/* ELIMINA BICICLETA MEDIANTE ID */


exports.bicicleta_delete = function (req,res){

  let BiciId = req.body.bicicletaID;

    Bicicleta.removebyId(BiciId, function (err, Bici){

        if(err) res.status(500).send(err.message); // Si err == TRUE
        res.status(201).send(Bici);

        
    })
};



exports.bicicleta_findById = function (req,res){

    let BiciId = req.body.bicicletaID;

    Bicicleta.findById(BiciId, function (err, Bici){
        if(err) res.status(500).send(err.message); 
        res.status(200).json({
            bicicletas: Bici
        })
    })
};



/* ACTUALIZA BICICLETA POR ID */

exports.bicicleta_update = function (req,res){

    let aBici = [ // Introducimos todos los valores mediante array, en este caso no es recomendable debido al acceso de este
        req.body._id,
        req.body.bicicletaID,
        req.body.color,
        req.body.modelo,
        req.body.ubicacion];
    
    Bicicleta.updateById(aBici, function(err, NewBici){

            if(err) res.status(500).send(err.message); // Si err == TRUE
            res.status(201).send(NewBici);
        }); 
};


