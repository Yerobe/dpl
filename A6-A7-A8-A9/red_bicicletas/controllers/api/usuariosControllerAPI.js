
let Usuario = require("../../models/Usuario");





exports.usuarios_list = function(req,res){    
    Usuario.allUsuarios(function(err,usuario){
        if(err) res.status(500).send(err.message);
        res.status(200).json({
            usuarios: usuario
        })
    })
    };



    exports.usuarios_create = function(req,res){
        let usuario = new Usuario({ // Declaramos los parametros obtenidos por el cuerpo de Postman en una nueva clase con sus propiedades
            nombre: req.body.nombre
        });
        Usuario.add(usuario,function(err,newUsuario){ // Llamamos al Modelo con el objeto Bici, y las respuestas CoolBack
            if(err) res.status(500).send(err.message); // Si err == TRUE
            res.status(201).send(newUsuario); // En caso de Código 201, imprime la Bici Creada
        });
    };



    exports.usuarios_delete = function (req,res){

      
          Usuario.removebyId(req.body, function (err, Usuario){
      
              if(err) res.status(500).send(err.message); // Si err == TRUE
              res.status(201).send(Usuario);
      
              
          })
      };




















exports.usuarios_reservar = function(req,res){ 

    Usuario.findById(req.body._id, function(err,usuario) {
   
    if(err) res.status(500).send(err.message);
   
    console.log(usuario);
   
    usuario.reservar(req.body.bici_id, req.body.desde, req.body.hasta, function(err){
   
    console.log("Reservada!!");
   
    res.status(200).send();
   
    });
   
    }); 
   
   };