let Bicicleta = require("../models/Bicicleta");

/* Definición del método */

exports.bicicleta_list = function(req,res){

    Bicicleta.allBicis(function(err,bicis){
        if(err) res.status(500).send(err.message);
        res.render("bicicletas/index",{bicis: bicis});

    })
}




exports.bicicleta_delete_post = function(req,res) { // Eliminacion por Id


    Bicicleta.removebyId(req.body,function(err,bicis){

        if(err) res.status(500).send(err.message);
        
        res.redirect("/bicicletas"); // Redirecciona a la página principal

    })

   }



   exports.bicicleta_update_get = function(req,res) {

    Bicicleta.findById(req.params.id,function(err,bici){ // 2
    if(err) res.status(500).send(err.message);
    res.render("bicicletas/update", {bici}); // Nos redirige al archivo pug de actualización

}); // Recibimos el Id

}





exports.bicicleta_update_post = function(req,res){
    
    let aBici = [ // Introducimos todos los valores mediante array, en este caso no es recomendable debido al acceso de este
        req.params.id,
        req.body.id,
        req.body.color,
        req.body.modelo,
        ubicacion =  [req.body.latitud,req.body.longitud]
    ];

    Bicicleta.updateById(aBici, function(err, NewBici){

        if(err) res.status(500).send(err.message); // Si err == TRUE
        res.redirect("/bicicletas"); // Redirecciona a la página principal
    });




}


exports.bicicleta_create_get = function(req,res){
    res.render("bicicletas/create"); // Redirecciona al archivo pug que contiene el formulario con los atributos
}



exports.bicicleta_create_post = function (req,res){
    
    let bici = new Bicicleta({ // Declaramos los parametros obtenidos por el cuerpo de Postman en una nueva clase con sus propiedades
        bicicletaID: req.body.id,
        color: req.body.color,
        modelo: req.body.modelo
    });

    bici.ubicacion = [req.body.latitud,req.body.longitud];

    Bicicleta.add(bici,function(err,bicis){

        if(err) res.status(500).send(err.message);
        
        res.redirect("/bicicletas"); // Redirecciona a la página principal

    }); // Introduce en el modelo Bicicleta los valores estimados

    
}
























