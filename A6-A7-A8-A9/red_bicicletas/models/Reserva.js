let mongoose = require("mongoose"); // Importacion
let Schema = mongoose.Schema; // Esquemas


let reservaSchema = new Schema ({

    desde: Date,
   
    hasta: Date,
   
    bicicleta: {type: mongoose.Schema.Types.ObjectId, ref: "Bicicleta"},
   
    usuario: {type: mongoose.Schema.Types.ObjectId, ref: "Usuario"}
   
   });


      /* Mostrar Todas las Reservas */

      reservaSchema.statics.allReservas = function (cb) { 
        return this.find({}, cb);
    };



/* Borrar Reserva */

reservaSchema.statics.removebyId = function(aReserva, cb) { // Nos elimina un registro especificando ID
    return this.remove({_id: aReserva._id},cb);

};



reservaSchema.statics.updateById = function(aReserva,cb) { // Nos elimina un registro especificando ID

    let Id = aReserva._id;

    return this.updateOne({_id: Id},{$set: {usuario: aReserva.usuario , bicicleta: aReserva.bicicleta , desde: aReserva.desde , hasta: aReserva.hasta}}, cb);

};





   
   
   
   //Cuántos días está reservada la bicicleta
   
   reservaSchema.methods.diasDeReserva = function() {
   
    return moment(this.hasta.diff(moment(this.desde), "days") + 1);
   
   };



   module.exports = mongoose.model ("Reserva", reservaSchema);