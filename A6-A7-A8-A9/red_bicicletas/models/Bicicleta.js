let mongoose = require("mongoose"); // Importacion
let Schema = mongoose.Schema; // Esquemas

let bicicletaSchema = new Schema({ // Creación
 bicicletaID: Number,
 color: String,
 modelo: String,
 ubicacion: { type: [Number], index: true }
});



bicicletaSchema.statics.allBicis = function (cb) { // Nos muestra todas las Bicicletas
    return this.find({}, cb);
};

bicicletaSchema.statics.add = function(aBici, cb) { // Nos agrega una nueva Bicicleta - coolback
    return this.create(aBici, cb);
};


bicicletaSchema.statics.removebyId = function(aBici, cb) { // Nos elimina un registro especificando ID
        return this.remove({_id: aBici.id},cb);

};

bicicletaSchema.statics.findById = function(aBici,cb) { // Nos filtra especificando un ID
  
    return this.findOne({_id: aBici},cb);

};



bicicletaSchema.statics.updateById = function(aBici,cb) { // Nos elimina un registro especificando ID

    let Id = aBici[0];

    return this.updateOne({_id: Id},{$set: {bicicletaID: aBici[1] , color: aBici[2] , modelo: aBici[3] , ubicacion: aBici[4]}}, cb);

};




/* Creación del Modelo */

/*
let Bicicleta = function (id, color, modelo, ubicacion) {
    this.id = id;

    this.color = color;

    this.modelo = modelo;

    this.ubicacion = ubicacion;

}*/

/*

Bicicleta.allBicis = [];

// Métodos relacionados a Bicicletas 
/*
Bicicleta.add = function (bici){
    this.allBicis.push(bici);
}

Bicicleta.removeById = function (aBiciId){
    for(let i = 0; i < Bicicleta.allBicis.length; i++){ // Recorre el Array
        if(Bicicleta.allBicis[i].id == aBiciId){ // Localiza el Id dentro del Array
            Bicicleta.allBicis.splice(i,1); // Splice nos borra el registro
        }
    }
}


Bicicleta.findById = function(aBiciId){
    let aBici = Bicicleta.allBicis.find(x => x.id == aBiciId); // Nos devuelve el Id del Array en el caso de que se encuentra
    if(aBici){ // Si existe
        return aBici; // Nos retona el Id
    }else{
        return false;}
}

Bicicleta.updateById = function (id, color, modelo, latitud, longitud){

    let aBici = Bicicleta.findById(id);

    if(aBici){ // Si existe
        aBici.color = color; // Introducimos el nuevo valor
        aBici.modelo = modelo; //  Introducimos el nuevo valor
        aBici.ubicacion = [latitud, longitud]; // Introducimos el nuevo valor como Array
        return true;
    }


}






// Agregación de Bicicletas a la clase Bicicleta 'Hardcoded'  

let a = new Bicicleta(1, "Rojo", "Trek", [28.503789, -13.853296]);

let b = new Bicicleta(2, "Azul", "Orbea", [28.501367, -13.853476]);

let c = new Bicicleta(3, "Negro", "Yero", [28.501867, -13.853175]);

Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.add(c);
*/














module.exports = mongoose.model ("Bicicleta", bicicletaSchema);

// module.exports = Bicicleta;

