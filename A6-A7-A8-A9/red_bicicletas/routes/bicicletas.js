var express = require('express'); // Incluimos Express
var router = express.Router(); // Hacemos uso la utilidad Router incluida en Express
let bicicletaController = require('../controllers/bicicleta'); // Cargamos el controlador para el modelo

router.get("/", bicicletaController.bicicleta_list);

router.get("/create",bicicletaController.bicicleta_create_get);

router.post("/create",bicicletaController.bicicleta_create_post); // Create por método POST - [Empujar]

router.post("/:id/delete",bicicletaController.bicicleta_delete_post)  // POST de eliminación al Id brindado

router.get("/:id/update",bicicletaController.bicicleta_update_get)  // Get, mostrando los valores de este Id seleccionado y llamamos al controlador estimado

router.post("/:id/update",bicicletaController.bicicleta_update_post) // Update recibiendo al Id seleccionado y llamamos al controlador estimado


module.exports = router; // Exportamos el módulo para su accesibilidad general
