let express = require('express');
let router = express.Router();
let usuariosControllerAPI = require("../../controllers/api/usuariosControllerAPI");


router.get("/", usuariosControllerAPI.usuarios_list);
router.post("/create", usuariosControllerAPI.usuarios_create);
router.delete("/eliminar", usuariosControllerAPI.usuarios_delete);

router.post("/reservar", usuariosControllerAPI.usuarios_reservar); // Dado por la Actividad

module.exports = router;