let express = require('express');
let router = express.Router();
let reservasControllerAPI = require("../../controllers/api/reservasControllerAPI");


router.get("/", reservasControllerAPI.reservas_list);
router.delete("/eliminar", reservasControllerAPI.reservas_delete);

router.put("/update", reservasControllerAPI.reserva_update);


module.exports = router;